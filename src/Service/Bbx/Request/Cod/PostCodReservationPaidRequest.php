<?php

declare(strict_types=1);

namespace App\Service\Bbx\Request\Cod;

use App\Infrastructure\ApiClient\Request\PostMethod;

final class PostCodReservationPaidRequest extends PostMethod
{
    private const ENDPOINT = 'bbx/cod/paid';

    /**
     * @param array<string, mixed> $data
     */
    public function __construct(
        private array $data
    ) {
    }

    public function getEndpoint(): string
    {
        return self::ENDPOINT;
    }

    /**
     * @inheritDoc
     */
    public function getData(): ?array
    {
        return $this->data;
    }
}
