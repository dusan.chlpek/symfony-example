<?php

declare(strict_types=1);

namespace App\Service\Bbx;

use App\Entity\Payment;
use App\Infrastructure\ApiClient\AdapterInterface;
use App\Infrastructure\ApiClient\Exception\ApiClientException;
use App\Infrastructure\ApiClient\Traits\LogsRequests;
use App\Service\Bbx\DataObject\Request\Cod\CodReservationPaid;
use App\Service\Bbx\DataObject\Response\Cod\CodReservationPaidResult;
use App\Service\Bbx\Request\Cod\CodRequestFactory;
use Psr\Log\LoggerInterface;
use DPD\Enum\Payment\PaymentStatus;
use DPD\Enum\Payment\PaymentReferenceType;
use DateTime;

final class BbxCodService
{
    use LogsRequests;

    public function __construct(
        private AdapterInterface $adapter,
        private CodRequestFactory $codRequestFactory,
        private LoggerInterface $externalLogger
    ) {
        $this->initLogger($this->externalLogger);
    }

    /**
     * @throws ApiClientException
     */
    public function postCodReservationPaid(
        CodReservationPaid $codReservationPaid
    ): CodReservationPaidResult {
        $request = $this->codRequestFactory->createPostCodPaidRequest(
            $codReservationPaid->toArray()
        );
        $this->logRequest($request);
        $response = $this->adapter->call($request);
        $this->logResponse($response);

        if ($response->isError()) {
            throw new ApiClientException($response->getResponseData(), $response->getHttpCode());
        }

        $responseData = json_decode($response->getResponseData(), true);

        return CodReservationPaidResult::fromArray($responseData);
    }

    final public function updateBbxBox(Payment $payment, string $newStatus): Payment
    {
        $this->logger->info('Starting updateBbxBox($newStatus = ' . $newStatus . ')');
        $this->logger->info('ReferenceNumber = ' . $payment->getReferenceNumber());
        $this->logger->info('CurrencyCode = ' . $payment->getCurrencyCode());
        $this->logger->info('Amount = ' . $payment->getAmount());
        $this->logger->info('Typ = ' . $payment->getReferenceType());

        // dam info bbx ze to je zaplatene ak nebol predtym a BbxDateInfo je null
        if ($newStatus == PaymentStatus::RESERVED) {
            if (
                $payment->getReferenceType() == PaymentReferenceType::PARCEL
                && $payment->getBbxDateInfo() === null
            ) {
                try {
                    $this->postCodReservationPaid(
                        new CodReservationPaid(
                            $payment->getReferenceNumber(),
                            $payment->getCurrencyCode(),
                            $payment->getAmount()
                        )
                    );

                    $payment->setBbxDateInfo(new DateTime());
                } catch (ApiClientException $e) {
                    $this->logger->error('Could not run bbx reservation paid [' .
                        $payment->getReferenceNumber() . ']: ' . $e->getMessage());
                }
            }
        }

        return $payment;
    }
}
