<?php

namespace App\Service\SKData\DataObject\Response\Scans;

final class Receiver
{
    public function __construct(
        private ?string $name,
        private ?string $street,
        private ?string $houseNr,
        private ?string $zip,
        private ?string $city,
        private ?string $countryCode
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['name'] ?: null,
            $data['street'] ?: null,
            $data['houseNr'] ?: null,
            $data['zip'] ?: null,
            $data['city'] ?: null,
            $data['countryCode'] ?: null,
        );
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @return string|null
     */
    public function getHouseNr(): ?string
    {
        return $this->houseNr;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }
}
