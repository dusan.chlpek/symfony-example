<?php

namespace App\Service\SKData\DataObject\Response\Scans;

final class Scan
{

    public const SCAN_COMPLETE = 13;
    public const SCAN_CANCELED = 6;

    public const RETURN_CODES = [
        '298',
        '300'
    ];

    public function __construct(
        private string $code,
        private string $dateTimeUtc,
        private string $parcelNr,
        private ?string $soCode,
        private ?string $asCode,
        private string $creationDateTimeUtc,
        private ?string $modificationDateTimeUtc,
        private ?Receiver $receiverDetail,
        private ?Cod $codDetail,
        private ?string $courierId
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['code'],
            $data['dateTimeUtc'],
            $data['parcelNr'],
            $data['soCode'],
            $data['asCode'],
            $data['creationDateTimeUtc'],
            $data['modificationDateTimeUtc'] ?: null,
            isset($data['receiverDetail']) ? Receiver::fromArray($data['receiverDetail']) : null,
            isset($data['codDetail']) ? Cod::fromArray($data['codDetail']) : null,
            $data['courierId'] ?: null
        );
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getSoCode(): ?string
    {
        return $this->soCode;
    }

    /**
     * @return string
     */
    public function getDateTimeUtc(): string
    {
        return $this->dateTimeUtc;
    }

    /**
     * @return string|null
     */
    public function getModificationDateTimeUtc(): ?string
    {
        return $this->modificationDateTimeUtc;
    }

    /**
     * @return string
     */
    public function getCreationDateTimeUtc(): string
    {
        return $this->creationDateTimeUtc;
    }

    /**
     * @return string
     */
    public function getParcelNr(): string
    {
        return $this->parcelNr;
    }

    /**
     * @return bool
     */
    public function wasReturnedToSender(): bool
    {
        return $this->getCode() == self::SCAN_CANCELED && in_array($this->getSoCode(), self::RETURN_CODES, true);
    }

    /**
     * @return bool
     */
    public function wasDelivered(): bool
    {
        return $this->getCode() == self::SCAN_COMPLETE;
    }

    /**
     * @return string
     */
    public function getCourierId(): string
    {
        return $this->courierId;
    }

}
