<?php

namespace App\Service\SKData\DataObject\Response\Scans;

final class Cod
{
    public function __construct(
        private float $amount,
        private string $currencyCode,
        private string $paymentType
    ) {
    }

    public static function fromArray(array $data): self
    {
        return new self(
            (float) $data['amount'],
            $data['currencyCode'],
            $data['paymentType'],
        );
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @return string
     */
    public function getPaymentType(): string
    {
        return $this->paymentType;
    }
}
