<?php

declare(strict_types=1);

namespace App\Service\SKData\Request\Scans;

use App\Service\SKData\Request\Scans\GetScansModifiedSinceRequest;
use DateTime;

final class ScansRequestFactory
{
    public function createGetScansModifiedSinceRequest(
        DateTime $dateTime,
        int $page = 1,
        int $perPage = 100
    ): GetScansModifiedSinceRequest {
        return new GetScansModifiedSinceRequest(
            $dateTime,
            $page,
            $perPage
        );
    }
}
