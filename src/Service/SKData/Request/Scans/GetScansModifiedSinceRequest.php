<?php

declare(strict_types=1);

namespace App\Service\SKData\Request\Scans;

use App\Infrastructure\ApiClient\Request\GetMethod;
use DateTime;

final class GetScansModifiedSinceRequest extends GetMethod
{
    private const ENDPOINT = 'scans/modified-since/%s/scan-types/06,13/%d/%d';

    public function __construct(
        private DateTime $dateTime,
        private int $page = 1,
        private int $perPage = 100
    ) {
    }

    public function getEndpoint(): string
    {
        return sprintf(
            self::ENDPOINT,
            $this->dateTime->format('YmdHis'),
            $this->page,
            $this->perPage
        );
    }
}
