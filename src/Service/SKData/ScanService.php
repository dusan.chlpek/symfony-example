<?php

declare(strict_types=1);

namespace App\Service\SKData;

use App\Infrastructure\ApiClient\AdapterInterface;
use App\Infrastructure\ApiClient\Exception\ApiClientException;
use App\Infrastructure\ApiClient\Traits\LogsRequests;
use App\Service\SKData\DataObject\Response\Scans\Scan;
use App\Service\SKData\Request\Scans\ScansRequestFactory;
use DateTime;
use Psr\Log\LoggerInterface;

final class ScanService
{
    use LogsRequests;

    public function __construct(
        private AdapterInterface $adapter,
        private ScansRequestFactory $scansRequestFactory,
        private LoggerInterface $externalLogger,
    ) {
        $this->initLogger($this->externalLogger);
    }

    /**
     * @throws ApiClientException
     * @return array<Scan>|null
     */
    public function getScansModifiedSince(
        DateTime $dateTime,
        int $page = 1,
        int $perPage = 100
    ): ?array {

        $request = $this->scansRequestFactory->createGetScansModifiedSinceRequest(
            $dateTime,
            $page,
            $perPage
        );

        $this->logRequest($request);
        $response = $this->adapter->call($request);
        $this->logResponse($response);

        if ($response->isError()) {
            throw new ApiClientException($response->getResponseData(), $response->getHttpCode());
        }

        $responseData = json_decode($response->getResponseData(), true);

        if (empty($responseData)) {
            return null;
        }

        return array_map(
            static fn (array $entity) => Scan::fromArray($entity),
            $responseData
        );
    }
}
