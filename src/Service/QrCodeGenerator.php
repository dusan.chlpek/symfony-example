<?php

declare(strict_types=1);

namespace App\Service;

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Exception;

final class QrCodeGenerator
{
    private PngWriter $writer;
    public const SERVER_QR_CODE_PATH = '../public/images/';
    public const SERVER_URL_CODE_PATH = 'images/';
    private const CASHLESS_ENDPOINT_URL = 'api/cashless/qr/';

    public const SUBDIR_DATE_FORMAT = 'Ym';

    private const PERMISSIONS = 0775;

    public function __construct(private string $baseurl)
    {
        $this->writer = new PngWriter();
    }

    /**
     * @throws Exception
     */
    public function generateCashlessValidationCode(
        string $content,
        string $encoding = 'UTF-8'
    ): string {
        $qrCode = QrCode::create($content)
            ->setEncoding(new Encoding($encoding))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $this->writer->write($qrCode);

        $qrCodeFileName = $this->getFileName($content);

        $result->saveToFile($this->getServerImagePath() . $qrCodeFileName);

        //$this->writer->validateResult($result, $content);

        return $this->getUrlImagePath($qrCodeFileName);

        // return base64_encode($result->getString());
    }

    private function getServerImagePath(): string
    {
        $imagePath = self::SERVER_QR_CODE_PATH . date(self::SUBDIR_DATE_FORMAT) . '/';

        if (!file_exists($imagePath)) {
            mkdir($imagePath, self::PERMISSIONS, true);
        }

        return $imagePath;
    }

    private function getUrlImagePath(string $fileName): string
    {
        return $this->baseurl . self::CASHLESS_ENDPOINT_URL . $fileName;
    }

    /**
     */
    private function getFileName(string $content): string
    {
        return time() . '_' . md5($content) . '.png';
    }
}
