<?php

declare(strict_types=1);

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Handler\Cashless\GetQrImageHandler;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route(path="/api/cashless/qr/{file}", methods={"GET"})
 */
final class QrController extends BaseController
{
    public function __construct(
        LoggerInterface $apiLogger,
        private GetQrImageHandler $handler
    ) {
        $this->logger = $apiLogger;
    }

    /**
     */
    public function __invoke(string $file, Request $request): Response
    {
        $this->logRequest($request);

        $pathImage = $this->handler->handle($file);
        $this->logger->info('Get QR image. filename: ' . $pathImage);

        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('content-type', 'image/png');

        if (file_exists($pathImage)) {
            $fileContent = file_get_contents($pathImage);

            if ($fileContent) {
                $response->setContent($fileContent);
                return $response;
            }
        }

        throw new RuntimeException('Image was not found');
    }
}
