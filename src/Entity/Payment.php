<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\EntityTrait\CreatedTrait;
use App\Entity\EntityTrait\UpdatedTrait;
use App\Entity\EntityTrait\UUIDTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use DPD\Enum\Payment\PaymentStatus;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="payment")
 * @ORM\HasLifecycleCallbacks()
 */
class Payment
{
    use UUIDTrait;
    use CreatedTrait;
    use UpdatedTrait;

    /**
     * @psalm-var Collection<int, PaymentTransaction>
     * @ORM\OneToMany(targetEntity="App\Entity\PaymentTransaction", mappedBy="payment")
     */
    private Collection $paymentTransactions;

    /**
     * @ORM\Column(type="string")
     */
    private string $referenceType;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private string $referenceNumber;

    /**
     * @ORM\Column(type="string")
     */
    private string $amount;

    /**
     * DateTime|null
     */
    public function getBbxDateInfo(): ?DateTime
    {
        return $this->bbxDateInfo;
    }

    /**
     */
    public function setBbxDateInfo(?DateTime $bbxDateInfo): void
    {
        $this->bbxDateInfo = $bbxDateInfo;
    }

    /**
     * @ORM\Column(type="string")
     */
    private string $currencyCode;

    /**
     * @ORM\Column(type="string")
     */
    private string $status;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private ?DateTime $syncedToCOD = null;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private ?DateTime $bbxDateInfo = null;

    public function __construct()
    {
        $this->uuid = Uuid::uuid4();
        $this->paymentTransactions = new ArrayCollection();
    }

    /**
     * @psalm-return Collection<int, PaymentTransaction>
     */
    public function getPaymentTransactions(): Collection
    {
        return $this->paymentTransactions;
    }

    public function addPaymentTransaction(PaymentTransaction $paymentTransaction): void
    {
        if (!$this->paymentTransactions->contains($paymentTransaction)) {
            $this->paymentTransactions->add($paymentTransaction);
            $paymentTransaction->setPayment($this);
        }
    }

    public function getReferenceType(): string
    {
        return $this->referenceType;
    }

    public function setReferenceType(string $referenceType): void
    {
        $this->referenceType = $referenceType;
    }

    public function getReferenceNumber(): string
    {
        return $this->referenceNumber;
    }

    public function setReferenceNumber(string $referenceNumber): void
    {
        $this->referenceNumber = $referenceNumber;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getSyncedToCOD(): ?DateTime
    {
        return $this->syncedToCOD;
    }

    public function setSyncedToCOD(DateTime $syncedToCOD): void
    {
        $this->syncedToCOD = $syncedToCOD;
    }

    public function getLastPaymentTransaction(): ?PaymentTransaction
    {
        $criteria = Criteria::create()->orderBy(['created' => 'desc']);
        $paymentTransactions = new ArrayCollection($this->getPaymentTransactions()->toArray());
        $paymentTransaction = $paymentTransactions->matching($criteria)->first();
        return $paymentTransaction !== false ? $paymentTransaction : null;
    }


    /**
     * @param array<string, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        $payment = new self();

        $payment->setReferenceType($data['referenceType']);
        $payment->setReferenceNumber($data['referenceNumber']);
        $payment->setAmount($data['amount']);
        $payment->setCurrencyCode($data['currencyCode']);
        $payment->setStatus($data['status'] ?? PaymentStatus::NEW);

        return $payment;
    }
}
