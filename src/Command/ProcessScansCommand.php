<?php

declare(strict_types=1);

namespace App\Command;

use App\Exception\PaymentStrategyNotFoundException;
use App\Exception\PaymentTransactionNotFoundException;
use App\Infrastructure\ApiClient\Exception\ApiClientException;
use App\Manager\FetchTimestampManager;
use App\Manager\PaymentManager;
use App\Service\CodTransaction\TransactionBatchService;
use App\Service\SKData\DataObject\Response\Scans\Scan;
use App\Service\SKData\ScanService;
use App\Traits\HandlesPayments;
use DateTime;
use App\Entity\FetchTimestamp;
use App\Entity\Payment;
use App\Exception\CodTransactionAlreadyExistsException;
use App\Infrastructure\Constants\PaymentChannel;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use DPD\Enum\Payment\PaymentReferenceType;
use DPD\Enum\Payment\PaymentStatus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

final class ProcessScansCommand extends Command
{
    use HandlesPayments;

    private const SCAN_COUNTS = 100;

    public function __construct(
        private string $returnEmail,
        private PaymentManager $paymentManager,
        private ScanService $scanService,
        private FetchTimestampManager $timestampManager,
        private TransactionBatchService $transactionBatchService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('cashless:process-scans')
            ->setDescription('Fetch and process scans of parcel for cashless by scan types from SKData (Shipper)'
                . 'Example: bin/console skdata:process-scans -- \'-5 days\' \'-5 minutes\'')
            ->addArgument('from', InputArgument::REQUIRED, 'date from')
            ->addArgument('to', InputArgument::REQUIRED, 'date to')
            ->addArgument(
                'disable-paging',
                InputArgument::OPTIONAL,
                'Disables pagination usage during requests to external systems',
                false
            );
    }

    /**
     * @throws ApiClientException
     */
    private function fetchAndProcessScans(
        DateTime $modifiedSince,
        bool $disablePagination,
        bool &$isLastPage,
        callable $processor
    ): ?DateTime {
        $page = 1;

        while (1) {
            $scans = $this->scanService->getScansModifiedSince(
                $modifiedSince,
                $page,
                self::SCAN_COUNTS
            );

            if ($scans === null) {
                return null;
            }

            foreach ($scans as $scan) {
                $processor($scan);
            }

            $count = count($scans);
            $hasMoreScans = $count === self::SCAN_COUNTS;

            if ($hasMoreScans && !$disablePagination) {
                $page++;
                continue;
            }

            if (!$hasMoreScans) {
                $isLastPage = true;
            }

            /** @var Scan $lastScan */
            $lastScan = $scans[$count - 1];
            $ts = $lastScan->getModificationDateTimeUtc();

            if (!$ts) {
                $ts = $lastScan->getCreationDateTimeUtc();
            }

            $newLastModificationDate = new DateTime($ts);
            // Allow pagination even for forcefully disabled pagination when last event in current request
            // has difference less than one second from $modifiedSince to as a protection against looping
            if ($disablePagination && $hasMoreScans && $modifiedSince->diff($newLastModificationDate)->s < 1) {
                $page++;
                continue;
            }

            return $newLastModificationDate;
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws UniqueConstraintViolationException|ApiClientException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $from = new DateTime($input->getArgument('from'));
        $to = new DateTime($input->getArgument('to'));
        $disablePagination = (bool)$input->getArgument('disable-paging');

        /** @var array<Payment> $payments */
        $payments = $this->paymentManager->findByStatusAndCreationDateAndReferenceType(
            PaymentStatus::RESERVED,
            PaymentReferenceType::PARCEL,
            $from,
            $to
        );
        $timestamp = $this->timestampManager->findByCodeOrCreate(FetchTimestamp::CODE_SKDATA_SCANS);

        $self = $this;
        $lastFetched = $timestamp->getLastFetched();

        if ($lastFetched === null) {
            $lastFetched = new DateTime();
            $timestamp->setLastFetched($lastFetched);
        }

        // force disabled pagination if last successful run of this command was more than one day old
        // reason is explained below
        if ($lastFetched->diff(new DateTime())->days > 1) {
            $disablePagination = true;
        }

        // Reason for this loop's existence is that for some unknown reason SKData will kill any
        // requests from epay that have bigger date range than 1 day after few pages requested.
        // This is another way of doing pagination without relying on SKData's unreliable
        // internal pagination system. Please don't hate me.
        $isLastPage = false;
        do {
            $lastModifiedSince = $this->fetchAndProcessScans(
                $lastFetched,
                $disablePagination,
                $isLastPage,
                static function (Scan $scan) use ($payments, $self, $output): void {

                    if (!in_array($scan->getCode(), [Scan::SCAN_COMPLETE, Scan::SCAN_CANCELED])) {
                        return;
                    }

                    $payment = $self->findPaymentForScan($scan->getParcelNr(), $payments);

                    if ($payment) {
                        $oldStatus = $payment->getStatus();
                        try {
                            $self->processScan($scan, $payment);

                            $output->writeln(
                                sprintf(
                                    '[%s]: updated payment %s with status %s => %s',
                                    date('H:i:s d.m.Y'),
                                    $payment->getReferenceNumber(),
                                    $oldStatus,
                                    $payment->getStatus()
                                )
                            );
                        } catch (CodTransactionAlreadyExistsException $e) {
                            $output->writeln(
                                sprintf(
                                    '[%s]: payment %s already in queue to COD',
                                    date('H:i:s d.m.Y'),
                                    $payment->getReferenceNumber(),
                                )
                            );
                        } catch (Throwable $e) {
                            $output->writeln(
                                sprintf(
                                    '[%s]: failed to update payment %s due to: %s',
                                    date('H:i:s d.m.Y'),
                                    $payment->getReferenceNumber(),
                                    $e->getMessage()
                                )
                            );
                        }
                    }
                }
            );

            if (!$lastModifiedSince) {
                return Command::FAILURE;
            }

            $timestamp->setLastFetched($lastModifiedSince);
            $this->timestampManager->save($timestamp, true);
            $lastFetched = $lastModifiedSince;
        } while ($disablePagination && !$isLastPage);

        return Command::SUCCESS;
    }

    /**
     * @param array<Payment>  $payments
     * @SuppressWarnings(UnusedPrivateMethod)
     */
    private function findPaymentForScan(string $parcelNumber, array $payments): ?Payment
    {
        foreach ($payments as $payment) {
            if ($payment->getReferenceNumber() === $parcelNumber) {
                return $payment;
            }
        }

        return null;
    }

    /**
     * @throws PaymentStrategyNotFoundException
     * @throws PaymentTransactionNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @SuppressWarnings(UnusedPrivateMethod)
     */
    private function processScan(Scan $scan, Payment $payment): void
    {
        $status = null;

        // When parcel was returned to sender, cancel payment
        if ($scan->wasReturnedToSender()) {
            $status = PaymentStatus::CANCELED;
        }

        if ($scan->wasDelivered()) {
            $status = PaymentStatus::COMPLETED;
        }

        if ($status === null) {
            return;
        }

        $this->updatePaymentStatus(
            [
                'status' => $status,
                'message' => 'Updated by an scan',
            ],
            $payment,
            $this->returnEmail,
            $this->paymentManager,
            PaymentChannel::CASHLESS
        );

        if ($status === PaymentStatus::COMPLETED) {
            try {
                $this->transactionBatchService->saveTransaction(
                    $payment,
                    $scan->getCourierId(),
                    null,
                );
            } catch (CodTransactionAlreadyExistsException) {
                // silent catch
            }
        }
    }
}
